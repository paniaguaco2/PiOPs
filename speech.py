#!/usr/bin/env python3

import argparse
import os
import os.path
import random
import sys
import subprocess


class ispeak:
    # PRIVATE FUNCTIONS
    def __init__(self, path_to=''):
        """
        Argument -> directory path of topics
        """
        if path_to == '':
            self.nodir = True
        else:
            self.nodir = False
            self.path_topics = os.path.abspath(path_to + "/topics")
            self.path_mp3 = os.path.abspath(path_to + "/mp3")
            if not os.path.exists(self.path_topics):
                os.makedirs(self.path_topics)
            if not os.path.exists(self.path_mp3):
                os.makedirs(self.path_mp3)
            self.__update_topics()
            self.__update_mp3()
            self.__set_volume(50)


    def __set_volume(self, volume):
        self.volume = volume
        os.popen("amixer sset PCM " + str(volume) + "%")


    def __get_sentences(self, topic):
        if topic == "all":
            for root, dirs, files in os.walk(self.path_topics):
                sentences = []
                for file in files:
                    with open(self.path_topics + "/" + file) as file:
                        sentences = sentences + [line for line in file]
        else:
            with open(self.path_topics + "/" + topic) as file:
                sentences = [line for line in file]
        return sentences


    def __get_random_sentence(self, topic):
        return random.choice(self.__get_sentences(topic))


    def __update_topics(self):
        for root, dirs, files in os.walk(self.path_topics):
            self.topics =    files


    def __update_mp3(self):
        for root, dirs, files in os.walk(self.path_mp3):
            self.mp3 =    files


    def __play_file(self, file):
        subprocess.call(["mplayer", file], stdout=subprocess.PIPE, stderr=subprocess.PIPE)


    def __play_text(self, text):
        url = ("http://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&q=" +
               text.replace(" ","+").replace("\n","") + "&tl=es")
        subprocess.call(["mplayer", "-speed", "1.2", url], stdout=subprocess.PIPE, stderr=subprocess.PIPE)


    def random_volume(self, volume=None):
        if not volume:
            volume = random.randint(90,100)
        self.__set_volume(volume)


    def set_volume(self, volume=50):
        self.__set_volume(volume)


    def get_volume(self):
        return self.volume


    def get_topics(self):
        if self.nodir:
            raise ArgumentTypeError("Topics directory is not defined")
            return []
        self.__update_topics()
        return self.topics


    def get_mp3(self):
        if self.nodir:
            raise ArgumentTypeError("MP3 directory is not defined")
            return []
        self.__update_mp3()
        return self.mp3


    def get_sentences(self, topic="all"):
        if self.nodir:
            raise ArgumentTypeError("Topics directory is not defined")
            return ""
        return self.__get_sentences(topic)


    def play_message(self, text):
        self.__play_text(text)


    def play_random(self, topic="all"):
        if self.nodir:
            raise ArgumentTypeError("Topics directory is not defined")
        else:
            self.__play_text(self.__get_random_sentence(topic))


    def play_mp3(self, file):
        if self.nodir:
            raise ArgumentTypeError("Topics directory is not defined")
        else:
            if file in self.mp3:
                self.__play_file(os.path.join(self.path_mp3,file))
            elif os.path.exists(file) and file[-3:] in ("mp3", "ogg"):
                self.__play_file(file)
            else:
                print("MP3 doesn't exist")



if __name__ == "__main__":
    mydir = os.path.dirname(os.path.abspath(sys.argv[0]))
    reproduce = ispeak(mydir)
    default_topic = random.choice(reproduce.get_topics())

    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--text", type=str, help="Text to reproduce")
    parser.add_argument("-r", "--random", type=str, nargs="?", const=default_topic,
                        help="Reproduce random sentence from ./topics/RANDOM. Without argument, \
                                reproduce random sentence from random topic")
    parser.add_argument("-l", "--list", action="store_true", help="List available topics")
    parser.add_argument("-m", "--mp3", type=str,
                        help="MP3 file of list. User -l to see available mp3")
    args = parser.parse_args()
    args = vars(args)

    if args["text"]:
        text = args["text"]
        reproduce.play_message(text)
        exit

    if args["random"]:
        topic = args["random"]
        reproduce.play_random(topic)
        exit

    if args["list"]:
        print ("This is the list of available topics:")
        print(reproduce.get_topics())
        print ("")
        print ("This is the list of available mp3:")
        print(reproduce.get_mp3())
        exit

    if args["mp3"]:
        reproduce.play_mp3(args["mp3"])
        exit
