#!/usr/bin/env python3

import speech
import telepot
import json
import argparse
import sys
import os.path

from datetime import datetime

def lee_fichero(configfile):
    with open(configfile, 'r') as f:
        return json.load(f)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config_file", required=True, help="Define el fichero json de configuracion del script")
    args = parser.parse_args()
    args = vars(args)

    if os.path.isfile(args["config_file"]):
        secretos = lee_fichero(args["config_file"])
        unsupported = "-1001293493454"

        today = datetime.now()
        pani_day = datetime(2019, 7, 24, 0, 0, 0)
        diferencia = today - pani_day
        days_after_pani = diferencia.days

        message = "Día %i después de Pani" %days_after_pani

        reproduce = speech.ispeak()
        telegram = telepot.Bot(secretos["token"])

        reproduce.play_message(message)
        telegram.sendMessage(unsupported, message.upper())
    else:
        print("El fichero de configuracion indicado no es un archivo vallido")
        sys.exit(1)
