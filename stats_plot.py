#!/usr/bin/env python

import sys
import string
import os
import os.path
import random

import numpy as np
import matplotlib.pyplot as plt


def get_random_string(stringLength=10):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


def create_graph(users, interacts, title=None):
    if len(users) > 0 and len(interacts) > 0:
        pos_y = np.arange(len(users))
        plt.cla()
        plt.barh(pos_y, interacts, align="center")
        plt.yticks(pos_y, users)
        # plt.xlabel('Interacciones')
        if title:
            plt.title(title)
        graph_file = "/tmp/" + get_random_string() + ".png"
        plt.savefig(graph_file)
        return graph_file
    else:
        print("ERROR al crear grafico")
        sys.exit(1)

def delete_graph__file(filename):
    if os.path.exists(filename):
        os.remove(filename)
