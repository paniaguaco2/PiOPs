# speech.py

## Prerequisitos

Para ejecutar el script *speech.py* e necesario instalar las siguientes dependencias

```bash
sudo apt-get install python3 python3-pip mplayer
sudo pip3 install -r requirements.txt
```

## Ejecución

```
usage: speech.py [-h] [-t TEXT] [-r [RANDOM]] [-l] [-m MP3]
optional arguments:
  -h, --help            show this help message and exit
  -t TEXT, --text TEXT  Text to reproduce
  -r [RANDOM], --random [RANDOM]
                        Reproduce random sentence from ./topics/RANDOM.
                        Without argument, reproduce random sentence from
                        random topic
  -l, --list            List available topics
  -m MP3, --mp3 MP3     MP3 file of list. User -l to see available mp3
  ```

# datio_ops.py

## Prerequisitos

```bash
pip3 install -r requirements.txt
```

## Configuración

Es necesario tener un fichero *json* con el token del bot de telegram y la lista de ids de telegram autorizados. El formato debe ser el siguiente:

```json
{
  "token": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  "admin": [
    "1111111",
    "4444444"
  ],
  "authorized_ids": [
    "1111111",
    "2222222",
    "3333333",
    "4444444",
    "5555555",
    "6666666"
  ],
  "moderation_mode": false
}
```

## Instalación

```bash
cd /home/pi
git clone https://github.com/ohermosa/PiOPs.git
cd PiOPs
sudo cp datiops_bot.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl start datiops_bot
```

## Ejecución

```
usage: datiops_bot.py [-h] -c CONFIG_FILE -s STAT_FILE

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG_FILE, --config_file CONFIG_FILE
                        Define el fichero json de configuracion del script
  -s STAT_FILE, --stat_file STAT_FILE
                        Define el fichero de estadisticas
```

## Logs

```bash
journalctl -xlfu datiops_bot
```

# send_stats.py

## Estadísticas

Cada vez que un usuario interactúa con el bot para reproducir un texto o un mp3, queda registrado en el fichero de estadísticas *stats.json*, el cual tiene el siguiente formato:

```json
{
  "2019": {
    "28": {
      "1111111": 49,
      "2222222": 7,
      "3333333": 1,
      "4444444": 61,
      "5555555": 19
    },
    "29": {
      "1111111": 78,
      "4444444": 8,
      "5555555": 50,
      "6666666": 1,
      "2222222": 1
    },
    "27": {
      "4444444": 1,
      "5555555": 12
    },
    "30": {
      "5555555": 15,
      "1111111": 9,
      "2222222": 3
    }
  }
}
```

Para cada semana del año, queda reflejado el número de interacciones de cada usuario de telegram con el bot. Esta información se usa en el script *send_stats.py* para enviar gráficas semanales del uso del bot

```bash
usage: send_stats.py [-h] -c CONFIG_FILE -s STAT_FILE

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG_FILE, --config_file CONFIG_FILE
                        Define el fichero json de configuracion del script
  -s STAT_FILE, --stat_file STAT_FILE
                        Define el fichero de estadisticas
```
